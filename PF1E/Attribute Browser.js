var options = templateOptions(),
	initialEntity = token?.actor ?? actor;

const css = 
`div.attribContainer {
	display: grid; grid: 0.5fr 1fr 1fr / 0fr 0fr 1fr;
	height: 100%
}

.attribList {
	grid-area: 2 / 3 / row3-end / 3;
	overflow-y: scroll;
	list-style-type: '@';
}

.attribList li {
	min-height: 20px;
	display:none;
}

.attribHead {
	grid-area: 1 / 3 / 1 / 3;
}
.attribResult {
	text-align:center;
}
ul.attribList {
	text-align:center;
}

`;
injectCSS();
	
if (!initialEntity) (Actor.create({name: "Temp Guy", type: "character"}, {temporary: true})).then(a => newDialog(a));
else newDialog(initialEntity);

function newDialog(entity, presetValue) {
	let data;
	if (entity instanceof Actor) data = {items: toEntries(entity.getRollData()), name: entity.name};
	else if (entity instanceof Item && entity.parentActor) data = {items: toEntries(entity.getRollData()), name: entity.parentActor.name + ": " + entity.name};
	else return;
	
	var d = new Dialog({
		title: entity.name,
		content: _templateCache['attribBrowse/main'](data, options),
		buttons: { 
			'refresh': {
				icon: '<i class="fas fa-sync"></i>',
				callback: (htm) => {
				  let currentVal = htm[0].querySelector('.attribInput')?.value ?? '';
				  d.close();
				  newDialog(entity, currentVal);
				}
			}
		},
		render: (htm) => {
			htm[0].setAttribute("style", "overflow-y: auto");
			htm[2].setAttribute("style", "max-height: 80px");
			htm.find('.visToggle').click(_visToggle.bind(this));
			htm.find('.copyPath').click(_copyPath.bind(this));
			htm.find('.attribInput').on("blur", (ev) => _calcValue(ev, entity));
			if (presetValue) {
				let input = htm.find('.attribInput')[0];
				input.value = presetValue;
				$(input).blur();
			}
			htm[0].addEventListener("drop", (ev) => _onDrop(ev, d));

			htm.find('ul.attribList>li').css("display", "list-item");
		}
	}, {height: Math.round(window.innerHeight * .75), width: Math.round(window.innerWidth * .25)});
	d.render(true);
};

function _onDrop(event, dialog) {
	event.preventDefault();
    let dropData;
    try {
      dropData = JSON.parse(event.dataTransfer.getData("text/plain"));
		if (dropData.type == "Item") {
			let currentVal = event.currentTarget.querySelector('.attribInput')?.value ?? '';
			dialog.close();
			newDialog(game.actors.get(dropData.actorId).items.get(dropData.data._id), currentVal);
	  }  
	if (dropData.type == "Actor") {
		let currentVal = event.currentTarget.querySelector('.attribInput')?.value ?? '';
		dialog.close();
		newDialog(game.actors.get(dropData.id), currentVal);
		}
    } catch (err) {
      return false;
    }
}

function templateOptions() {
	if(!_templateCache['attribBrowse/list']) {
		const listCompile = Handlebars.compile(
`{{#each items}}
<li>
	<a class="rollable copyPath">{{name}}</a>
	{{#if items}}
	<a class="rollable visToggle"><i class="fas fa-eye-slash"></i></a>
	<ul style="list-style: none outside;">
	{{> attribBrowse/list}}
	</ul>
	{{/if}}
	{{#if value}}
	: {{value}}
	{{/if}}
</li>
{{/each}}`
		);
		Handlebars.registerPartial('attribBrowse/list', listCompile);
		_templateCache['attribBrowse/list'] = listCompile;
	}
	if(!_templateCache['attribBrowse/main']) {
		const mainCompile = Handlebars.compile(
`<div class="attribContainer">
<div class="attribHead">
	<h1>{{name}}</h1>
	<input class="attribInput" type="text"></input>
	<h1 class="attribResult">
</div>
<ul class="attribList">
	{{> attribBrowse/list}}
</ul></div>`
		);
		Handlebars.registerPartial('attribBrowse/main', mainCompile);
		_templateCache['attribBrowse/main'] = mainCompile;
	}
	return { allowProtoMethodsByDefault : true, allowProtoPropertiesByDefault: true };
}

function toEntries(obj) {
    var initial = Object.entries(obj);
    return initial.map(parsed => {
    if (parsed[1] instanceof Array)
        return {name: parsed[0], value: '[...]'};
	if (typeof parsed[1] == "string")
		return {name: parsed[0], value: `"${parsed[1]}"`};
    if (parsed[1] instanceof Object)
        return {name: parsed[0], items: toEntries(parsed[1])};
    return {name: parsed[0], value: '' + parsed[1]};
    });
}

function _copyPath(event) {
	event.preventDefault();
	const el = event.target;
	var path = el.innerText,
		closest = el.parentNode.parentNode.closest('li')?.querySelector('a.copyPath');
	while (closest) {
		path = closest.innerText + '.' + path;
		closest = closest.parentNode.parentNode.closest('li')?.querySelector('a.copyPath');
	}
	path = '@' + path;
	navigator.clipboard.writeText(path);
	return;
}

function _calcValue(event, entity) {
	const el = event.target;
	var result = "";
	try { result = new Roll(el.value, entity.getRollData()).roll().total + ""; }
	catch (err) { result = "err"; }
	el.nextElementSibling.innerText = result;
}

function _visToggle(event) {
	event.preventDefault();
	const el = event.target.closest('a'),
		status = el.children[0].classList.contains('fa-eye');
	el.children[0].className = 'fas fa-eye' + (status ? '-slash' : '');
	[...el.nextElementSibling.children].forEach(o => o.setAttribute('style', 'min-height: 20px; ' + (status ? 'display:none;' : 'display:list-item')));
}

function injectCSS() {
	if (!window.attribBrowseCss) {
		var style = document.createElement('style');
		style.type = 'text/css';
		style.innerHTML = css;
		document.getElementsByTagName('head')[0].appendChild(style);
		window.attribBrowseCss = true;
	}
}