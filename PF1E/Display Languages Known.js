// Computes languages on selected tokens OR all PCs if none selected
// groupBy can be "char" or "language"
var groupBy = "char";
//END CONFIG

let chars = (canvas.tokens.controlled.length > 0 ? canvas.tokens.controlled.map(o => o.actor) : game.actors).filter(o => o.permission == 3),
	msgText = '<div style="display: grid; grid-template: auto / 1fr 1fr;">',
	charData = [],
	groups = {},
	groupHeaders = [];

//Pull and format languages/ customLanguages
charData = chars.map(ch => {
	let name = ch.name,
	languages = Object.values(ch.data.data.traits.languages).reduce((acc, cur) => {
		if (typeof cur == 'object')
			acc.push(...cur.map(o => CONFIG.PF1.languages[o] ?? o));
		else if (typeof cur == 'string' && cur != '')
			acc.push(...cur.split(/\s*;\s*/g));
		return acc;
	}, []);
	return {name: name, languages: languages};
});

groups = charData.reduce((acc, obj) => {
	if (groupBy == 'char')
		acc[obj.name] = obj.languages;
	else {
		obj.languages.forEach(o => {
			if (!acc[o]) acc[o] = [];
			acc[o].push(obj.name);
		});
	}
	return acc;
}, {});

groupHeaders = Object.keys(groups).sort();
groupHeaders.forEach((cat, ind) => {
	if (ind % 2 == 0 && ind == groupHeaders.length - 1)
		msgText += `<div style="grid-column-end: span 2">`;
	else
		msgText += '<div>';
	msgText += `<h2>${cat}</h2><p style="padding-right: .5rem">${groups[cat].join(', ')}</p></div>`;
});

ChatMessage.create({
	flavor: 'Languages Known',
	speaker: ChatMessage.getSpeaker(),
	content: 	(msgText + '</div>'),
	type: CONST.CHAT_MESSAGE_TYPES.OTHER,
	whisper: ChatMessage.getWhisperRecipients('gm').map(o => o.id)
});