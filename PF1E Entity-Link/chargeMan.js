/*-			DOCUMENTATION			-*/
// Syntax: [<Modifier>] Quantity [from/to <Item Name>]
// [Use/Set/Gain/Reset] Quantity
// Quantity
//		Number:			Accepts math and rolldata (ie. ceil(@item.lvl, 2))
// Modifier
//		Use:			Reduce charge by Quantity, if no charges, reduces item quantity by Quantity
//		Consume:		Reduces charge by Quantity
//		Remove:			Reduces item quantity by Quantity
//		Gain:			Increase charge by Quantity, if no charges, increase item quantity by Quantity
//		Charge:			Increases charge by Quantity
//		Add:			Increases item quantity by Quantity
//		Reset:			Sets Charge back to max charge. If Quantity is provided, modifies maxcharge by Quantity

/*-			CONFIGURATION			-*/
const c = {
	commands: {
		prep: {
			toFrom: ['To', 'From']
		},
		modifier: 
	}
}

/*-			COMMAND					-*/
try {
	var inputText = window.macroChain?.pop() || event.target.parentNode?.innerText.trim(),
		chatMessage = game.messages.get(event.target.closest('.message')?.getAttribute('data-message-id')),
		targetActor = game.actors.get(chatMessage?.data.speaker?.actor) ?? game.user.character ?? canvas.tokens.controlled[0]?.actor,
		preps = [].concat(...Object.values(c.commands.prep)),
		argParse = new RegExp(`^(\S+) ?(.*?) ?((?:${preps.join('|')}) [^#]*)(?: ?#(.*))?$`,'i'),
		[modifier, quantity, prepList, type] = inputText.match(argParse).slice(1);
	if (modifier) modifier = actionType.toLowerCase();
}
catch (err) {
	console.log(err, "Whatever you did didn't work");
}
(() => {
	if (targetActor) {
		if (!targetActor.hasPerm(game.user, "OWNER")) return ui.notifications.warn(game.i18n.localize("PF1.ErrorNoActorPermission"));
			const item = targetActor.items.find(o => {
				debugger;
				if (actionType && actionType != o.type) return false;
				return o.name == actionName;
		});
		if (!item) return ui.notifications.warn(`You don't have an item named ${actionName}`);

		if (!game.keyboard.isDown("Control"))
			item.use({skipDialog: (game.settings.get("pf1", "skipActionDialogs") !== game.keyboard.isModifierActive("Shift"))});
		else
			item.roll();
	}
})();