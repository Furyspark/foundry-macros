/*-			DOCUMENTATION			-*/
// Syntax: <Operator> <Buff Name> [to/from/on <targets>] [as <Alt Name>] [at <Level>] [for <Length>] [in <distance>]
// Apply/Remove/Toggle/Delete/Activate Buff Name to/from/on selected,target,Character Names at Level in distance
// ie @Macro[applyBuff]{Apply Mage Armor to selected,-myself as Lightest Armor at @cl-1}
// Operator: 
//		Apply:			Create and Activate Buff
//		Remove: 		Delete and Deactivate Buff
//		Toggle: 		Applies if off, deactivates if on
//		Activate:		Turn on buff but only if present
//		Deactivate:		Create and turn off buff if it doesn't exist
//		Change:			Does nothing but update level
// Buff Name:
//		Will use the first Buff from the compendium. If compendiumPriority is specified below, will check those compendiums first, then pf1.commonbuffs.
//		Doesn't short circuit so every compendium provided might be checked
// Targets (Comma separated, "-" in front negates, same rules as Alt Name [see below]):
//		selected: 		Default if argument is provided. User who clicks the button
// 		Character Name:	No commas in the name. Everything else is okay. *Only updates linked actors (by design - convince me otherwise)*
//		target(s)(ed):	All targets of the user who clicks the button
//		template:		All character tokens within the last template of the user who clicks the button
//		me/myself:		Character the card containing the button came from
//		speaker:		Active speaker of the person who clicks the card
//		friendly/ies:	Tokens with disposition "friendly"
//		neutral(s):		Tokens with disposition "neutral" (GM only unless subtracting)
//		hostile(s):		Tokens with disposition "hostile" (GM only unless subtracting)
//		PC(s):			PC tokens
//		NPC(s):			NPC tokens (GM only unless subtracting)
//		uuid:			Generally used internally for the time being. Prefix uuids with '#'
// Distance:
//		number. Unit is purely decoration. Just compares with current scene.
//		This acts as a filter so it needs to be used in conjunction with a target selector above
// Alt Name:
//		Will search for both alt name and original. Quotes won't work here so use underscores(_) as spaces if you need to use any of the keywords (to,from,on,as,at)
// Level
//		number			Sets item level if supplied.
//      @cl-[1,2,3,s]	Accepts rollData strings. @cl-1, @cl-2, @cl-3, @cl-s are shorthands for @attributes.spells.spellbooks.X.cl.total
//		->				Will use the next inline roll results instead. Useful for pretty lines. If inside a "label", will use the value inside there
// Length
//		formula			Set formula to anything before unit. Use @@ anywhere to delay evaluation until buff calculation
//		unit			[Turn(s), Round(s), Minute(s), Hour(s)]
// Supports label mode. @Macro[applyBuff]{Apply Mage Armor to selected,-myself as Lightest Armor at ->::Activate Sparkle Barrier}[[@cl]]

/*-			CONFIGURATION			-*/
const c = {
	compendiumPriority: ["world.mybuffs", "pf-content.pf-buffs"],
}

/*-			COMMAND					-*/
try {
	if (typeof event === "undefined") event = new Event("none");
	if (event instanceof MessageEvent) var socketData = event.data ? JSON.parse(event.data.replace(/[^{([]+/,""))?.[0].result[0] : null;
	var inputText = window.macroChain?.pop() || (typeof args == "object" ? args.join(" ") : false) || event.target?.closest?.('button,a')?.textContent.trim(),
		macroId = this.id,
		chatMessage = socketData ? game.messages.get(socketData._id) : game.messages.get(event.target?.closest?.('.message')?.getAttribute('data-message-id')),
		argParse = /^([aA]pply|[rR]emove|[tT]oggle|[aA]ctivate|[dD]eactivate|[cC]hange) "?([^\n]*?)"?( (?:to|from|on|as|at|for)(?=([^"\\]*(\\.|"([^"\\]*\\.)*[^"\\]*"))*[^"]*$) (?:.*))?$/,
		operator, buffName, modString,
		compCollNames = c.compendiumPriority.concat(['pf1.commonbuffs']),
		myself = event.args?.[1] ?? game.actors.get(chatMessage?.data.speaker?.actor) ?? game.user.character ?? canvas.tokens.controlled[0]?.actor,
		sourceItem = event.args?.[0] ?? chatMessage?.itemSource,
		targets,
		modList,
		targetActors = [],
		excludedActors = [],
		altName,
		levelOverride,
		durationFormula,
		durationUnit,
		distance;

	
	if (event.target?.closest?.('button,a')?.dataset.extra)
		inputText = event.target.closest('button,a').dataset.extra;
	[operator, buffName, modString] = inputText.match(argParse)?.filter(o => o).slice(1,4) ?? [];
	if (!buffName || !operator) throw ui.notifications.error(`${game.i18n.localize('PF1.BuffName')}: "${buffName}"`);
	if (modString) {
		let rollData = sourceItem?.getRollData?.() ?? myself?.getRollData?.() ?? {};
		let shorthand = {	'cl-1': myself.data.data.attributes.spells.spellbooks.primary.cl.total,
							'cl-2': myself.data.data.attributes.spells.spellbooks.secondary.cl.total,
							'cl-3': myself.data.data.attributes.spells.spellbooks.tertiary.cl.total,
							'cl-s': myself.data.data.attributes.spells.spellbooks.spelllike.cl.total};
		modList = modString.split(/ (to|from|on|as|at|within|in|for) /).slice(1);
		for (var k = 0; k < modList.length; k += 2) {
			switch(modList[k]) {
				case 'to':
				case 'from':
				case 'on':
					targets = modList[k+1].replace(/_/g,' ');
					break;
				case 'as':
					altName = modList[k+1].replace(/_/g,' ');
					break;
				case 'at':
					if (modList[k+1].indexOf('->') > -1)
						modList[k+1] = event.target.nextElementSibling?.textContent.trim();
					levelOverride = modList[k+1];
					if (levelOverride.search(/[^\d]/) > -1) {
						levelOverride = RollPF.safeTotal(levelOverride, mergeObject(rollData, shorthand));
					}
					break;
				case 'within':
				case 'in':
					distance = parseInt(modList[k+1].match(/\d+/)?.[0] ?? 0);
					break;
				case 'for':
					[durationFormula, durationUnit] = modList[k+1].match(/(.+?) *(?:(Turn|Round|Minute|Hour)s?)/i)?.filter(o => o).slice(1) ?? [];
					if (durationFormula) {
						if (durationFormula.indexOf('@@') > -1)
							durationFormula = durationFormula.replace(/@@/g,'g');
						else
							durationFormula = RollPF.safeTotal(durationFormula, mergeObject(rollData, shorthand));
					}
					break;
			}
		}
	}
	if (!targets) targets = 'selected';
	
	(targets ? targets.split(',') : ['selected']).forEach(tar => {
		var accuActors;
		tar = tar.trim();
		if (tar.charAt(0) == '-') {
			accuActors = excludedActors;
			tar = tar.slice(1);
		}
		else
			accuActors = targetActors;
		switch(tar.toLowerCase()) {
			case 'selected':
				if (canvas.tokens.controlled.length == 0) ui.notifications.warn('No tokens selected');
				accuActors.push(...canvas.tokens.controlled.map(o => o.actor));
				break;
			case 'template':
				let temp = event.args?.[5] ?? chatMessage?.data.flags.pf1?.metadata?.template ?? canvas.templates.placeables.filter(o => o.data.user == game.userId)?.pop()?.id;
				if (!temp) ui.notifications.warn('No template found');
				else {
					let tokens = canvas.tokens.placeables.filter(o => canvas.grid.getHighlightLayer('Template.' + temp).geometry.containsPoint(o.center));
					accuActors.push(...tokens.map(o => o.actor));
				}
				break;
			case 'target':
			case 'targets':
			case 'targeted':
				if (chatMessage?.data.flags.pf1?.metadata?.targets.length)
					accuActors.push(...chatMessage.data.flags.pf1.metadata.targets.flatMap(o => canvas.tokens.get(o)?.actor));
				else if (game.users.find(o => o.character === myself))
					accuActors.push(...[...(game.users.find(o => o.character === myself).targets)].map(o => o.actor));
				else
					accuActors.push(...[...game.user.targets].map(o => o.actor));
				break;
			case 'myself':
			case 'me':
				accuActors.push(myself);
				break;
			case 'friendly':
			case 'friendlies':
				accuActors.push(...canvas.tokens.placeables.filter(o => o.data.disposition == 1).map(o => o.actor));
				break;
			case 'neutral':
			case 'neutrals':
				if (game.user.isGM || accuActors === excludedActors)
					accuActors.push(...canvas.tokens.placeables.filter(o => o.data.disposition == 0).map(o => o.actor));
				break;
			case 'hostile':
			case 'hostiles':
				if (game.user.isGM || accuActors === excludedActors)
					accuActors.push(...canvas.tokens.placeables.filter(o => o.data.disposition == -1).map(o => o.actor));
				break;
			case 'pcs':
			case 'pc':
				accuActors.push(...canvas.tokens.placeables.filter(o => o.actor.data.type == 'character').map(o => o.actor));
				break;
			case 'npcs':
			case 'npc':
				if (game.user.isGM || accuActors === excludedActors)
					accuActors.push(...canvas.tokens.placeables.filter(o => o.actor.data.type == 'npc').map(o => o.actor));
				break;
			case 'speaker':
				accuActors.push(game.pf1.ActorPF.getActiveActor());
				break;
			case 'remaining':
				tar = '#' + chatMessage?.data.flags.applyBuff?.remaining;
				if (tar.indexOf('undefined') > -1)
					break;
			default:
				//Name or uuid search
				if (tar.indexOf('#') != 0)
					accuActors.push(game.actors.find(o => o.name == tar));
				else {
					tar.split(',').forEach(uuid => {
						var [uScene, uToken, uActor] = uuid.match(/^#?(?:Scene\.([^.]*))?(?:\.Token\.([^.]*))?(?:\.?Actor\.([^.]*))?$/).slice(1);
						if (uActor)
							accuActors.push(game.actors.get(uActor));
						else if (uScene == canvas.id)
							accuActors.push(canvas.tokens.get(uToken).actor);
					});
				}
		}
	});

	if (distance) {
		let myTok = myself.getActiveTokens()[0];
		targetActors = targetActors.filter(o => {
			var targetTok = o.token ?? o.getActiveTokens()[0],
				ray, dist;
			if (targetTok) {
				ray = new Ray(myTok, targetTok);
				dist = canvas.grid.measureDistances([{ray}], {gridSpaces: true})[0];
				if (dist <= distance) return true;
			}
			excludedActors.push(o);
			return false;
		});
	}
	excludedActors = excludedActors.flatMap(o => o ? o.id : []);
	targetActors = [...new Set(targetActors)].filter(o => o && !excludedActors.includes(o.id));
	excludedActors = [];

	let found;
	found = game.items.find(o => o.type == "buff" && o.name.localeCompare(buffName, undefined, { sensitivity: "base", ignorePunctuation: true }) == 0);
	if (found)
		buffFound(found);
	else {
		found = game.pf1.utils.findInCompendia(buffName, {packs: compCollNames});
		if (found) {
			found.pack.getDocument(found.index._id).then(doc => buffFound(doc));
		}
		else {
			let foundOnChar;
			targetActors.find(a => {
				foundOnChar = foundOnChar || a.items.find(o => o.type == "buff" && o.name == buffName);
				return foundOnChar;
			});
			if (foundOnChar)
				buffFound(foundOnChar);
		}
	}
}
catch (err) {
	console.log(err, "Whatever you did didn't work");
}
async function buffFound(buff) {
	if (buff) buff = buff.toObject();
	if (typeof levelOverride != 'undefined' && buff) buff.data.level = levelOverride;
	if (typeof durationFormula != 'undefined' && typeof durationUnit != 'undefined' && buff) Object.assign(buff.data.duration, {value: durationFormula, units: durationUnit});
	if (typeof altName != 'undefined' && buff) buff.name = altName, buff.data.identifiedName = altName;
	targetActors.some(act => {
		if (act && act.isOwner) {
			let presentBuff = act.items.find(o => {return o.data.type == 'buff' && (o.name == buffName || o.name == altName);});

			if (!buff && presentBuff) {
				buff = {}; buff = mergeObject(presentBuff.toObject(), {}, {inplace: false});
				if (typeof levelOverride != 'undefined') buff.data.level = levelOverride;
				if (typeof durationFormula != 'undefined' && typeof durationUnit != 'undefined') Object.assign(buff.data.duration, {value: durationFormula, units: durationUnit});
				if (typeof altName != 'undefined') buff.name = altName, buff.data.identifiedName = altName;
			}
			let updateObject = {'data.identifiedName': altName ?? buff.name, 'name': altName ?? buff.name};
			if (typeof levelOverride != 'undefined') updateObject['data.level'] = buff.data.level;
			if (typeof durationFormula != 'undefined' && typeof durationUnit != 'undefined') Object.assign(updateObject, {'data.duration.value': durationFormula, 'data.duration.units': durationUnit});
			if (!buff) return ui.notifications.error(game.i18n.format('ERROR.lsNoItemFound', {item: buffName ?? altName})), true;
			switch(operator.toLowerCase()) {
				case 'apply':
					if (!buff.data.active)
						buff.data.active = true;
					
					if (presentBuff)
						presentBuff.update(Object.assign({'data.active': true}, updateObject));
					else
						act.createEmbeddedDocuments("Item", [buff]);			
					break;
				case 'remove':
					if (presentBuff)
						presentBuff.delete();				
					break;
				case 'toggle':
					if (presentBuff)
						presentBuff.update(Object.assign({'data.active': !getProperty(presentBuff.toObject(), 'data.active')}, updateObject));
					else {
						if (!buff.data.active)
							buff.data.active = true;
						
						act.createEmbeddedDocuments("Item", [buff]);
					}
					break;
				case 'activate':
					if (presentBuff)
						presentBuff.update(Object.assign({'data.active': true}, updateObject));
					break;
				case 'deactivate':
					if (presentBuff)
						presentBuff.update(Object.assign({'data.active': false}, updateObject));
					else
						act.createEmbeddedDocuments("Item", [buff]);
					break;
				case 'change':
					if (presentBuff)
						presentBuff.update(updateObject);
						break;
				default:
					return ui.notifications.warn("That's not how you use this. See documentation.");
			}
		}
		else if (act)
			excludedActors.push(act);
	});
	var successStr = 'No tokens affected.',
		affectedActors = targetActors.length - excludedActors.length;
	if (affectedActors != 0 && buff)
		successStr = `${buff.name} was changed on ${affectedActors} token${(affectedActors > 1 ? 's' : '')}.`;
	if (excludedActors.length > 0) {
		successStr += ' Requesting GM assistance for the rest.';
		var remainingModList = modList.filter((o,p) => !["to", "on", "from"].includes(o) && !["to", "on", "from"].includes(modList[p-1])),
			excludedNames = excludedActors.map(o => (o.token ? o.token.name : o.name)).join(', '),
			gmButtonTitle = `${operator} ${buffName} to remaining ${remainingModList.join(' ')}`.trim(),
			enrichedButton = `<a class="entity-link content-link" data-type="Macro" data-id="${macroId}"><i class="fas fa-terminal"></i> ${gmButtonTitle}</a>`,
	flavorText = `I'm trying to<br>${operator} ${buffName} ${remainingModList.join(' ')} to: ${excludedNames}`,
			remainingUUID = excludedActors.map(o => (o.token ? o.token.uuid : o.uuid)).join(','),
			spoofedRoll = RollPF.safeRoll(excludedActors.length.toString()).toJSON(),
			messageData;
		spoofedRoll.formula = '#Affected:';
        spoofedRoll = Roll.fromJSON(JSON.stringify(spoofedRoll));
		messageData = ChatMessage.applyRollMode({
			sound: null,
			flavor: flavorText,
			speaker: ChatMessage.getSpeaker({actor: myself}),
			content: `${enrichedButton}`,
			type: CONST.CHAT_MESSAGE_TYPES.ROLL,
			roll: spoofedRoll,
			flags: mergeObject(chatMessage?.data.flags ?? {}, {applyBuff: {remaining: remainingUUID}}, {inplace: false})
		}, "blindroll");
		ChatMessage.create(messageData);
	}
	ui.notifications.info(successStr.trim());
}