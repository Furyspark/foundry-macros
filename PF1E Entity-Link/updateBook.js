/*-			DOCUMENTATION			-*/
// Syntax: Make/Break/Transcribe [Override]
// Override(s): (comma delimited)
//		Class Name:		the name of a class the spellbook should be render.
//		Group Name:		the spell group the spellbook should use. [primary, secondary, tertiary, spelllike]
// Currently this mostly serves as a helper function for managing spell lists
// To use: Create an item with a description
// @Macro[updateBook]{Make}
// Spell Name 1
// Spell Name 2 ...(etc)
// And then save and click the button created.

/*-			CONFIGURATION			-*/
const c = {
	compendiumPriority: ["world.myspells", "pf1.spells"],
	commands: {
		make: 'Make',
		unmake: 'Break',
		update: 'Update',
		learn: 'Learn',
		sort: 'Sort',
		transcribe: 'Transcribe',
		overwrite: 'Overwrite'
	},
	levels: ['Cantrips/ Orisons', '1st Level', '2nd Level', '3rd Level',
		'4th Level', '5th Level', '6th Level', '7th Level', '8th Level', 
		'9th Level', '10th Level', 'Not Able']
}

/*-			COMMAND					-*/
try {
	var inputText = window.macroChain?.pop() || (typeof args == "object" ? args.join(" ") : false) || event.target.closest('button,a')?.textContent.trim(),
		macroId = this.id,
		htmlItem = event.target.closest('.item'),
		packList = [],
		lclz = Object.keys(c.commands).reduce((res, key) => {res[key] = c.commands[key].toLowerCase(); return res;}, {});

	if (htmlItem) {
		var actId, itId, act, it;
		if (htmlItem.nodeName == 'DIV')
			[actId, itId] = htmlItem.id.match(/actor-(.*)-item-(.*)/)?.slice(1) ?? [];
		else if (htmlItem.nodeName == 'LI')
			[actId, itId] = [htmlItem.closest('.actor')?.id.match(/actor-(.*)/)?.[1], htmlItem.getAttribute('data-item-id')];
		act = game.actors.get(actId);
		it = act?.items.get(itId) ?? game.items.get(itId);
		if (!it) {
			it = ui.windows[htmlItem.dataset.appid]?.object;
			act = it?.parentItem?.actor;
		}
		if (!it && !act.data.token.actorLink) {
			act = canvas.tokens.placeables.filter(o => o.actor.id == act.id).find(i => {
				let found = i.actor?.items.get(itId);
				if (found)
					it = found;
				return found
			}).actor;
		}
	}
	
	if (it && inputText)
		processCommand(inputText, it, act);
	else
		ui.notifications.error(`You can\'t use ${inputText} here.`);
}
catch(err) {
	console.error(err, "Whatever you did didn't work");
}

async function processCommand(command, item, actor) {
	var command_lc = command.toLowerCase(),
		spellList = [],
		selectedCommand = Object.entries(lclz).find(l => command_lc.indexOf(l[1]) > -1)?.[0],
		rider = command.slice(command_lc.indexOf(lclz[selectedCommand]) + lclz[selectedCommand].length);;
	if (!actor && selectedCommand.indexOf("make") == -1) return ui.notifications.error("Cannot find actor");
	c.compendiumPriority.forEach(c => game.packs.get(c)?.getIndex());
	switch(selectedCommand) {
		case "make":
			c.compendiumPriority.forEach(c => {
				let pack = game.packs.get(c);
				if (pack)
					packList.push(c);
			});
			makeBook(item, actor, packList, rider);
			break;
		case "unmake":
			unMakeBook(item, actor, rider);
			break;
		case "transcribe":
			transcribeBook(item, actor, rider);
			break;
		case "learn":
			learnBook(item, actor, rider);
			break;
		case "update":
			updateBook(item, actor, rider);
			break;
		case "overwrite":
			overwriteBook(item, actor, rider);	
			break;
		default:
			return ui.notification.error(`Unrecognized command "${selectedCommand}"`);
	}
}

function transcribeBook(item, actor, extras) {
	if (!extras || !/\S/.test(extras)) extras = ' Primary';
	if (extras.indexOf(',') > -1) return ui.notifications.error('Multiple overrides not supported at this time');
	var oldDesc = item.data.data.description.value,
		lowerExtra = extras.trim().toLowerCase(),
		reverseBooks = Object.fromEntries(Object.entries(actor.data.data.attributes.spells.spellbooks).filter(o => o[1].class != '' && o[1].class != '_hd').map(p => [p[1].class, p[0]])),
		spells = actor.items.filter(o => { return o.type == 'spell' && (o.data.data.spellbook == lowerExtra || o.data.data.spellbook == reverseBooks[lowerExtra]); } ),
		spellNames = spells.map(o => o.name),
		newDesc = `<p>@Macro[updateBook]{${c.commands.make + extras}}@Macro[updateBook]{${c.commands.transcribe + extras}}</p>\n<p>`;
	spellNames = [...new Set(spellNames)].sort((a,b) => a.localeCompare(b));
	newDesc += spellNames.join('<br />\n') + '</p>';
	item.update({'data.description.value': newDesc});
}

async function overwriteBook(item, actor, extras) {
	if (!extras || !/\S/.test(extras)) extras = ' Primary';
	if (extras.indexOf(',') > -1) return ui.notifications.error('Multiple overrides not supported at this time');
	var oldDesc = item.data.data.description.value,
		lowerExtra = extras.trim().toLowerCase(),
		reverseBooks = Object.fromEntries(Object.entries(actor.data.data.attributes.spells.spellbooks).filter(o => o[1].class != '' && o[1].class != '_hd').map(p => [p[1].class, p[0]])),
		bookKey = reverseBooks[lowerExtra] || lowerExtra,
		spells = actor.items.filter(o => { return o.type == 'spell' && o.data.data.spellbook == bookKey; } ),
		validReg = new RegExp("(?<!" + c.levels[11] + ".*)@Compendium\\[([^.]+\\.[^.]+)\\.(.*?)\\]{(.*?)}", 'g'),
		listNames = [...oldDesc.matchAll(validReg)],
		toRemove = spells.filter(o => !listNames.find(p => p[3] == o.name)).map(q => q.id),
		toAdd = listNames.filter(o => !spells.find(p => p.name === o[3])),
		addPromises = [];
	toAdd.forEach(o => addPromises.push(game.packs.get(o[1]).getDocument(o[2])));
	await actor.deleteEmbeddedDocuments("Item", toRemove);
	Promise.all(addPromises).then(adds => {
		actor.createEmbeddedDocuments("Item", adds.map(a => {
			return mergeObject(a.toObject(), {data: {spellbook: bookKey}});
		}));
	});
}

function learnBook(item, actor, extras) {
	if (!extras || !/\S/.test(extras)) extras = ' Primary';
	if (extras.indexOf(',') > -1) return ui.notifications.error('Multiple overrides not supported at this time');
	var oldDesc = item.data.data.description.value,
		lowerExtra = extras.trim().toLowerCase(),
		reverseBooks = Object.fromEntries(Object.entries(actor.data.data.attributes.spells.spellbooks).filter(o => o[1].class != '' && o[1].class != '_hd').map(p => [p[1].class, p[0]])),
		bookKey = reverseBooks[lowerExtra] || lowerExtra,
		spells = actor.items.filter(o => { return o.type == 'spell' && o.data.data.spellbook == bookKey; } ),
		validReg = new RegExp("(?<!" + c.levels[11] + ".*)@Compendium\\[([^.]+\\.[^.]+)\\.(.*?)\\]{(.*?)}", 'g'),
		listNames = [...oldDesc.matchAll(validReg)],
		toAdd = listNames.filter(o => !spells.find(p => p.name === o[3])),
		addPromises = [];
	debugger;
	toAdd.forEach(o => addPromises.push(game.packs.get(o[1]).getDocument(o[2])));
	Promise.all(addPromises).then(adds => {
		actor.createEmbeddedDocuments("Item", adds.map(a => {
			a.toObject();
			return mergeObject(a.toObject(), {data: {spellbook: bookKey}});
		}));
	});
}

async function updateBook(item, actor, extras) {
	if (!extras || !/\S/.test(extras)) extras = ' Primary';
	if (extras.indexOf(',') > -1) return ui.notifications.error('Multiple overrides not supported at this time');
	var oldDesc = item.data.data.description.value,
		lowerExtra = extras.trim().toLowerCase(),
		reverseBooks = Object.fromEntries(Object.entries(actor.data.data.attributes.spells.spellbooks).filter(o => o[1].class != '' && o[1].class != '_hd').map(p => [p[1].class, p[0]])),
		bookKey = reverseBooks[lowerExtra] || lowerExtra,
		spells = actor.items.filter(o => { return o.type == 'spell' && o.data.data.spellbook == bookKey; } ),
		validReg = new RegExp("(?<!" + c.levels[11] + ".*)@Compendium\\[([^.]+\\.[^.]+)\\.(.*?)\\]{(.*?)}", 'g'),
		listNames = [...oldDesc.matchAll(validReg)],
		toRemove = spells.filter(o => {
			let found = listNames.find(p => p[3] == o.name);
			if (found) {
				found.prep = o.data.data.preparation;
				found.domain = o.data.data.domain;
			}
			return found;
		}).map(q => q.id),
		toAdd = listNames.filter(o => spells.find(p => p.name === o[3])),
		addPromises = [];
	toAdd.forEach(o => {
		addPromises.push(game.packs.get(o[1]).getDocument(o[2]).then(p => {
			p = p.toObject();
			if (o.prep) p.data.preparation = o.prep;
			if (o.domain) p.data.domain = o.domain;
			return p;
		}));
	});
	await actor.deleteEmbeddedDocuments("Item", toRemove);
	Promise.all(addPromises).then(adds => {
		actor.createEmbeddedDocuments("Item", adds.map(a => {
			return mergeObject(a, {data: {spellbook: bookKey}});
		}));
	});
}

function unMakeBook(item, actor, extras) {
	var oldDesc = item.data.data.description.value,
		spellNames = [...oldDesc.matchAll(/@Compendium\[.*?\]{(.*?)}/g)].map(o => o[1]),
		newDesc = `<p>@Macro[updateBook]{${c.commands.make + extras}}@Macro[updateBook]{${c.commands.transcribe + extras}}</p>\n<p>`;
	spellNames = [...new Set(spellNames)].sort((a,b) => a.localeCompare(b));
	newDesc += spellNames.join('<br />\n') + '</p>';
	item.update({'data.description.value': newDesc});
}

function makeBook(item, actor, packs, extras) {
	var spellPromises = [],
		spellList = (new DOMParser()).parseFromString(item.data.data.description.value.replace(/<br ?\/?>/g, '\n'), 'text/html').body.innerText.split(/[\r\n]+/).slice(1),
		classOverrides = extras.trim().split(/\s*,\s*/g).map(o => o.toLowerCase()),
		brokenLinks = '';

	spellList = spellList.flatMap((sp, slIdx) => {
		let found = game.pf1.utils.findInCompendia(sp, {packs});
		if (found) {
			spellPromises.push(found.pack.getDocument(found.index._id));
			return [];
		}
		return /\S/.test(sp) ? sp : [];
	});
	if (spellList.length > 0) {
		ui.notifications.error('Couldn\'t find ' + spellList.join(', '));
		brokenLinks = spellList.map(s => '@Compendium[broke]{' + s + '}').join('<br />\n');
	}
	Promise.all(spellPromises).then(spells => {
		var newDesc = '',
			type = ['primary', 'secondary', 'tertiary', 'spelllike'],
			newType = [],
			groups, groupKeys;

		if (classOverrides[0] !== '') {
			classOverrides = classOverrides.map(over => {
				let pos = type.indexOf(over);
				if (pos < 0) return over;
				return actor.data.data.attributes.spells.spellbooks[type[pos]].class;
			});
			newType = classOverrides.filter(o => !['','_hd'].includes(o));
		}
		else
			newType = type.filter(t => !['','_hd'].includes(actor.data.data.attributes.spells.spellbooks[t].class)).map(o => actor.data.data.attributes.spells.spellbooks[o].class);
		
		groups = spells.reduce((obj,spell) => {
			var spellClasses = Object.fromEntries(spell.data.data.learnedAt.class),
				sorted = false;
			
			if (spellClasses.hasOwnProperty('sorcerer/wizard')) {
				spellClasses.sorcerer = spellClasses['sorcerer/wizard'];
				spellClasses.wizard = spellClasses['sorcerer/wizard'];
				delete spellClasses['sorcerer/wizard'];
			}
			if (spellClasses.hasOwnProperty('cleric/oracle')) {
				spellClasses.cleric = spellClasses['cleric/oracle'];
				spellClasses.oracle = spellClasses['cleric/oracle'];
				delete spellClasses['cleric/oracle'];
			}
			if (spellClasses.hasOwnProperty('unchained Summoner')) {
				spellClasses.summonerUnchained = spellClasses['unchained Summoner'];
				delete spellClasses['unchained Summoner'];
			}
			newType.forEach(t => {
				let lev = spellClasses[t.toLowerCase()];
				if (lev != undefined) {
					sorted = true;
					if (!obj.hasOwnProperty(t)) obj[t] = {};
					if (!obj[t].hasOwnProperty(lev)) obj[t][lev] = [];
					obj[t][lev].push(spell);
				}
			});
			if (!sorted) {
				if (!obj.hasOwnProperty('none')) obj['none'] = {'11': []};
				obj.none['11'].push(spell);
			}
			return obj;
		}, {});
		
		groupKeys = Object.keys(groups);
		groupKeys.forEach(g => {
			if (groupKeys.length > 1) {
				let catName = g;
				catName = catName.charAt(0).toUpperCase() + catName.slice(1);
				newDesc += `<h1>${catName}</h1>`;
			}
			let levels = Object.keys(groups[g]);
			levels.forEach(l => {
				newDesc += `<h2>${c.levels[l]}</h2><p style="display: flex; flex-wrap: wrap;">`;
				groups[g][l].sort((a, b) => a.name.localeCompare(b.name));
				newDesc += groups[g][l].map(sp => {
					return '@Compendium[' + sp.compendium.metadata.package + '.' + 
						sp.compendium.metadata.name + '.' + sp.id + ']{' + sp.name + '}';
				}).join(' ');
				newDesc += '</p>';
			});
		});
		let buttonStyle = 'display: flex; flex-grow: 1; min-width 33.33%; margin: .5em 0; justify-content: center;';
		newDesc += `<section class="secret"><p style="display: flex; flex-wrap: wrap; justify-content: space-evenly;">` +
			`<span style="${buttonStyle}">@Macro[updateBook]{${c.commands.learn + extras}}</span>` +
			`<span style="${buttonStyle}">@Macro[updateBook]{${c.commands.update + extras}}</span>` + 
			`<span style="${buttonStyle}">@Macro[updateBook]{${c.commands.overwrite + extras}}</span>` +
			`<span style="${buttonStyle}">@Macro[updateBook]{${c.commands.unmake + extras}}</span>` +
			`</p><p>${brokenLinks}</p></section>`;
		item.update({'data.description.value': newDesc});
	});
}

//Ar is array to be searched, el is element being looked for, compare_fn is comparison
//Returns index or insertion point as negative
function binarySearch(ar, el, compare_fn) {
	var m = 0;
	var n = ar.length - 1;
	while (m <= n) {
		var k = (n + m) >> 1;
		var cmp = compare_fn(el, ar[k]);
		if (cmp > 0) {
			m = k + 1;
		} else if(cmp < 0) {
			n = k - 1;
		} else {
			return k;
		}
	}
	return -m - 1;
}

function sortPack(inputArr) {
	let n = inputArr.length;
		for (let i = 1; i < n; i++) {
			let current = inputArr[i];
			let j = i-1;
			let currentLower = current.name.toLowerCase();
			while ((j > -1) && (currentLower.localeCompare(inputArr[j].name.toLowerCase(), undefined, {ignorePunctuation: true}) < 0)) {
				inputArr[j+1] = inputArr[j];
				j--;
			}
			inputArr[j+1] = current;
		}
	return inputArr;
}

function escapeRegExp(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}